import React, { useState } from 'react';
import { Input, Button, Col, Row } from "reactstrap";
const PageB = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const handleFirstNameChange = (event) => {
    setFirstName(event.target.value);
  };

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  const validateForm = () => {
    const errors = {};

    if (firstName.trim() === '') {
      errors.firstName = 'Vui lòng nhập tên!';
    }

    if (lastName.trim() === '') {
      errors.lastName = 'Vui lòng nhập họ!';
    }

    if (!isEmailValid(email)) {
      errors.email = 'Email không hợp lệ!';
    }

    if (password.trim() === '') {
      errors.password = 'Vui lòng nhập mật khẩu!';
    }

    setErrors(errors);
  };

  const isEmailValid = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    validateForm();

    const information = {
      firstName:firstName,
      lastName:lastName,
      email: email,
      password: password,
    };
    // Kiểm tra xem form có lỗi hay không
    if (Object.keys(errors).length === 0) {
      // Xử lý logic khi form hợp lệ
      console.log(information);
      console.log("First Name:", firstName);
      console.log("Last Name:", lastName);
      console.log("Email:", email);
      console.log("Password:", password);
    }
  };
  return (
    <div className='text-center'>
      <br></br>
      <h3 style={{ "color": "white" }}>Sign Up For Free</h3>
      <form  >
      <Row className='mt-4'>
        <Col xs="6">
          <Input className='form-control'
            name='firstName'
            placeholder='First Name *'
            style={{ "backgroundColor": "transparent", "color": "white" }}
            value={firstName}
            onChange={handleFirstNameChange}
           
          />
            {errors.firstName && <div style={{"color":"red","textAlign": "left"}}>{errors.firstName}</div>}
        </Col>
      

        <Col xs="6">
          <Input className='form-control '
            name='lastName'
            placeholder='Last Name *'
            style={{ "backgroundColor": "transparent", "color": "white" }}
            type="text"
            value={lastName}
            onChange={handleLastNameChange} />
             {errors.lastName && <p style={{"color":"red","textAlign": "left"}}>{errors.lastName}</p>}
        </Col>
      </Row>
      <Input className='form-control mt-4'
        name='email'
        placeholder='Email Address *'
        type='email'
        style={{ "backgroundColor": "transparent", "color": "white" }}
        value={email}
        onChange={handleEmailChange}
      />
       {errors.email && <p style={{"color":"red","textAlign": "left"}}>{errors.email}</p>}
      <Input className='form-control mt-4'
        name='password'
        placeholder='Password  *'
        type='password'
        style={{ "backgroundColor": "transparent", "color": "white" }} 
        value={password}
        onChange={handlePasswordChange}/>
        {errors.password && <p style={{"color":"red","textAlign": "left"}}>{errors.password}</p>}

      <div className='row mt-2'>
        <Button 
        className="btn btn-secondary"
          style={{ "backgroundColor": "#41b188" }}
          onClick={handleSubmit}>
          GET STARTED
        </Button>
      </div>
      </form>
    </div>
  );
};

export default PageB;
