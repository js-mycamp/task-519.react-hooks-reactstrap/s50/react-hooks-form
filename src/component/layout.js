import { useState } from "react";
import Login from "./login"
import SignUp from "./signUp"
import { Row, Button, ButtonGroup,Col } from "reactstrap";

function Layout() {
  

    const [type, setType] = useState("log-in");
    const handleChangeType = (type) => {
        setType(type === "log-in" ? "log-in" : "sign-up");
    };
    return (
        <div className="d-flex justify-content-center align-items-center  " >
 <Col xs={10} sm={6} md={6} lg={4} >
        
            <Row className="d-flex justify-content-center align-items-center row  " style={{ "backgroundColor": "#43535A","padding": "10px 10px" }}>
           
                <ButtonGroup>
                    <Button color="darkGrey" className="btn btn-secondary" onClick={() => handleChangeType("sign-up")}
                        style={type === "sign-up" ? { backgroundColor: "#41b188" } : {}}
                    >Sign Up</Button>
                    <Button color="darkGrey" className="btn btn-secondary" onClick={() => handleChangeType("log-in")}
                        style={type === "log-in" ? { backgroundColor: "#41b188" } : {}}
                    >Login </Button>
                </ButtonGroup>
                {type === "log-in" ? <Login   /> : <SignUp />}
              
            </Row>
            </Col>
            </div>

    )
}
export default Layout