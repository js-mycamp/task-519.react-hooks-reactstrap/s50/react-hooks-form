import React from "react";
import { Input, Button, Col, Row, Form } from "reactstrap";
import { useState } from "react";
const PageA = () => {

 

  // form
  const [inputEmail, setInputEmail] = useState("")
  const inputEmailChangeHandler = (event) => {
    setInputEmail(event.target.value)
  }

  const [inputPassword, setPassword] = useState("");

  const inputPasswordChangeHandler = (event) => {
    setPassword(event.target.value);
  };
  const [errors, setErrors] = useState({});
  // form
  // VAILIDATE
  const validateForm = () => {
    const errors = {};

    if (!isEmailValid(inputEmail)) {
      errors.inputEmail = "Email không hợp lệ!";
      console.log( "Email không hợp lệ!")
    }

    if (inputPassword.trim() === "") {
      errors.inputPassword = "Vui lòng nhập mật khẩu!";
      console.log( "Vui lòng nhập mật khẩu!")
    }

    setErrors(errors);
  };

  const isEmailValid = (email) => {
    // Kiểm tra định dạng email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const information = {
    email: inputEmail,
    password: inputPassword,
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    validateForm();

    // Kiểm tra xem form có lỗi hay không
    if (Object.keys(errors).length === 0) {
      // Xử lý logic khi form hợp lệ
      console.log(information);
      console.log("Email:", inputEmail);
      console.log("Password:", inputPassword);
    }
  };
  // VAILIDATE
  return (

    <div className="text-center">
      <br></br>
      <h3 style={{ "color": "white" }}>Welcome Back!</h3>
      <form onSubmit={handleSubmit} >
        <div>
          <Input className="form-control mt-4" 
          name="email" placeholder="Email Address *" 
          type="email"
          style={{ "backgroundColor": "transparent", "color": "white" }}

          onChange={inputEmailChangeHandler} value={inputEmail} />
          {errors.inputEmail && <p style={{"color":"red","textAlign": "left"}}>{errors.inputEmail}</p>}
          
        </div>
        <Input className="form-control mt-4" 
        name="password"
        placeholder="Password  *"
        type="password" 
        style={{ "backgroundColor": "transparent", "color": "white" }}

        onChange={inputPasswordChangeHandler} value={inputPassword} />
        {errors.inputPassword && <p style={{"color":"red","textAlign": "left"}}>{errors.inputPassword}</p>}
      

        <Row >
          <Col xs="4"></Col>
          <Col xs="4"></Col>
          <Col xs="4"> <a href="#" style={{ "color": "#41b188" }} >       Forgot Password?     </a></Col>

        </Row>

        <div className="row mt-2">
          <Button className="btn btn-secondary" 
          style={{ "backgroundColor": "#41b188" }}
          type="submit">
            LOG IN
          </Button>
        </div>
      </form>
    </div>
  );
};

export default PageA;
